from typing import Any, Callable

import pandas as pd


def pandas_pipable(f: Callable, *args, **kwargs) -> Callable[[Any], pd.DataFrame]:
    """
    decorator, aimed at making writing pandas 
    method chaining (pipes) **even more** ergonomic

    takes a function whose first argument is a pandas.DataFrame
    and calls that function with the given arguments

    if the function returns nothing, the initial input
    dataframe is returned, else the produced output.
    will raise error if the return type is not a dataframe

    the functions that use this decorator are supposed to
    be used by the 'pipe' method, as shown below

    ## Example
    
    >>> @pandas_pipable
    >>> def to_csv(df: pd.DataFrame, f:Path):
    >>>     df.to_csv(path_or_buf=f, index=False)
    >>>     # note that this returns None
    >>>
    >>> new_df = (
    >>>     old_df
    >>>     # do some manipulation
    >>>     .query(...)
    >>>     .select(...)
    >>>     .pipe(to_csv, outfile)
    >>>     # can do more stuff here
    >>>     .merge(...)
    >>> )
    >>> # new_df is still a dataframe, even if the 'to_csv' method
    >>> # and the original function both return None
    """
    def inner(*args, **kwargs) -> pd.DataFrame:
        result = f(*args, **kwargs)
        out = result or args[0]
        assert isinstance(out, pd.DataFrame)
        return out
    return inner